#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videoio/videoio.hpp>

class ball_detect{
    public:
        ball_detect(){
            int deviceID = 0;
            int apiID = cv::CAP_ANY;

            int frame_widht = 1280;
            int frame_height = 720;

            int low_hue = 0;
            int high_hue = 30;

            int low_sat = 200;
            int high_sat = 255;

            int low_val = 100;
            int high_val = 255;

            char Check_Esc_key = 0;

            cv::Mat frame;
            cv::Mat frame_HSV;
            cv::Mat frame_thresh;

            std::vector<cv::Vec3f> vector_img;
            
            cv::VideoCapture cap;
            cap.open(deviceID, apiID);
            cap.set(cv::CAP_PROP_FRAME_WIDTH, frame_widht);
            cap.set(cv::CAP_PROP_FRAME_HEIGHT, frame_height);

            if(!cap.isOpened())
            {
                std::cout << "No Input Detected!!" << std::endl;
            }

            while (Check_Esc_key != 27 && cap.isOpened())
            {
                bool success = cap.read(frame);
                if (!success)
                {
                    std::cout << "Error reading image" << std::endl;
                    break;
                }

                cv::cvtColor(frame , frame_HSV, CV_BGR2HSV);
                cv::inRange(frame_HSV,cv::Scalar(low_hue,low_sat,low_val),cv::Scalar(high_hue,high_sat,high_val), frame_thresh);
                cv::GaussianBlur(frame_thresh, frame_thresh, cv::Size(3,3),0);
                cv::dilate(frame_thresh, frame_thresh, 0);
                cv::erode(frame_thresh, frame_thresh, 0);

                cv::HoughCircles(frame_thresh, vector_img, CV_HOUGH_GRADIENT,2,frame_thresh.rows / 4,100,50,10,800);

                for (int i = 0; i < vector_img.size() ; i++)
                {
                    std::cout << "Object Location X = " << vector_img[i][0];
                    std::cout << "Object Location Y = " << vector_img[i][1];
                    std::cout << "Radius = " << vector_img[i][2];

                    cv::circle(frame, 
                    cv::Point((int)vector_img[i][0], (int)vector_img[i][1]), 3,
                    cv::Scalar(0,255,0), CV_FILLED);

                    cv::circle(frame, cv::Point((int)vector_img[i][0] , (int)vector_img[i][1]), 
                    (int)vector_img[i][2] , cv::Scalar(0,0,255));
                }
                
                cv::namedWindow("Original", cv::WINDOW_AUTOSIZE);
                cv::namedWindow("detected_image", cv::WINDOW_AUTOSIZE);

                cv::createTrackbar("LowH", "detected_image", &low_hue, 179);
                cv::createTrackbar("HighH", "detected_image", &high_hue, 179);

                cv::createTrackbar("LowS", "detected_image", &low_sat, 255);
                cv::createTrackbar("HighS", "detected_image", &high_sat, 255);

                cv::createTrackbar("LowV", "detected_image", &low_val, 255);
                cv::createTrackbar("HighV", "detected_image", &high_val, 255);

                cv::imshow("Original", frame);
                cv::imshow("Detection", frame_thresh);

                if (cv::waitKey(30)==27)
                {
                    std::cout << "Aborted" << std::endl;
                    break;
                }
            }
        }

        ~ball_detect()
        {
            cvDestroyWindow("camera_Output");
        }
};

int main(int argc, char** argv)
{
    ros::init(argc,argv,"ball_detect");
    ros::NodeHandle n;
    ball_detect cam_object;

    ROS_INFO("Cam Tested");
}